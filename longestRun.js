// {Longest Run
// Write a function that, given a string, finds the longest run of identical characters and returns an array containing the start and end indices of that run.
// If there are two runs of equal length, return the first one. Return [0,0] for no runs.}

function longestRun (string) {
  // Write your code here, and
  // return your final answer.
  let result = [0, 0];
  let index = -1;
  let array = [];

  if(string.length < 1) {
    return result;
  }

  string.split('').forEach(function(e, i, a) {
    if(e !== a[i - 1]) {
      index++;
      array.push([e]);
    } else {
      array[index].push(e);
    }
  });

  index = 0;

  array.forEach(function(e, i, a) {
    if(e.length > a[index].length) {
      index = i;
    }
  })

  if(array[index].length < 2) {
    return result;
  }

  if(index === 0) {
    return [0, array[index].length - 1];
  }

  for (var i = 0; i < index + 1; i++) {
    if(i === index) {
      result[1] += array[index].length + result[0] - 1
    } else {
      result[0] += array[i].length;
    }
  }

  return result;
}
