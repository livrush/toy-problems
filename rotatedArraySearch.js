// Rotated Array Search
// If you have a sorted array, a rotated version of that array is the sorted array rotated some number of times left or right. For example, a rotated version of [0, 1, 2, 3, 4] is [3, 4, 0, 1, 2] (rotated right twice). Each rotated array has a single pivot point where the groups of values to the left and right are sorted, but the sorting does not continue accross the pivot.
//
// Given a rotated, sorted array, your task is to efficiently find the index of an element within that array. Your time complexity goal is O( log(n) ), where ‘n’ is the number of elements in the array. For simplicity, you can assume that there are no duplicate elements in the array.

// I - Array, target
// O - index number or -1
// C -
// E -


// function rotatedArraySearch (rotated, target) {
//   // Write your code here, and
//   // return your final answer.
//   rotated.unshift(rotated.pop());
//   let result = arguments[2] || rotated.length - 1;
//   if (arguments[2] === 0) { result = 0; }
//   if (result === -1) {
//     return result;
//   } else if (rotated[0] === target) {
//     return result;
//   } else {
//     return rotatedArraySearch(rotated, target, result - 1);
//   }
// }

function rotatedArraySearch (rotated, target) {
  // Write your code here, and
  // return your final answer.
  rotated.unshift(rotated.pop());
  let result = arguments[2] || rotated.length - 1;
  if (arguments[2] === 0) { result = 0; }
  if (result === -1) {
    return result;
  } else if (rotated[0] === target) {
    return result;
  } else {
    return rotatedArraySearch(rotated, target, result - 1);
  }
}
