function spiralTraversal (matrix) {
  // Write your code here, and
  // return your final answer.

  if(matrix.length === 1) {
    return matrix[0];
  }

  let outerRim = function(array) {
    if(!array[0].length) {
      return [];
    }

    let top = array.splice(0, 1)[0];
    let bottom = array.splice(-1, 1)[0].r
    educeRight((res, curr) => res.concat([curr]), []);
    let rightSide = array.reduce((res, curr) => res.concat(curr.splice(-1, 1)), []);
    let leftSide = array.reduceRight((res, curr) => res.concat(curr.splice(0, 1)), []);
    let outer = top.concat(rightSide.concat(bottom.concat(leftSide)));
    if(array.length === 1) {
      return outer.concat(array.splice(0, 1)[0]);
    }
    if(array[0].length) {
      return outer.concat(outerRim(array));
    } else {
      return outer;
    }
  }

  let result = outerRim(matrix);

  return result;
}
