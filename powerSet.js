// {Power Set
// Return an array that contains the power set of a given string. The power set is a set of all the possible subsets, including the empty set.
//
// Make sure your code does the following:
//
// All characters in a subset should be sorted alphabetically, and the array of subsets should be sorted alphabetically.
// Sets of the same characters are considered duplicates regardless of order and count only once, e.g. ‘ab’ and ‘ba’ are the same.
// Duplicate characters in strings should be ignored; for example, ‘obama’ should be evaluated as if it only contained one ‘a’. See the result below.
//
// I - string
// O - array
// E -
// C - Each subset should be sorted alphabetically, final array should be sorted alphabetically}

function powerSet(string) {
  const results = [''];

  const uniq = function(array) {
    return array.reduce((uniqs, l) => uniqs.includes(l) ? uniqs : uniqs.concat([l]), []).sort();
  }

  const options = uniq(string.split(''));

  const manipulator = function(array, string) {
    if (array.length === 0) { return; }
    string += array[0];
    results.push(string);
    array.slice(1).forEach(function(e, i, a) {
      manipulator(a.slice(i), string);
    });
    manipulator(array.slice(1), '');
  }

  manipulator(options, '');

  return uniq(results);
}
