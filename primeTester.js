function primeTester (n) {
  let result = true;
  if (n === 1) {
    return false;
  }
  if (!(Math.sqrt(n) % 1)) {
    return false;
  }
  for (var i = 2; i < Math.sqrt(n); i++) {
    if (!(n % i)) {
      return false;
    }
  }
  return result;
}
