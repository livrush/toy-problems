// Deep Equality
// Write a function that, given two objects, returns whether or not the two are deeply equivalent–meaning the structure of the two objects is the same, and so is the structure of each of their corresponding descendants.
//
// DO NOT use JSON.stringify.

// I - Two objects
// O - Boolean
// C - No JSON.stringify
// E -

deepEquals = function(a, b){
  if(Array.isArray(a) || Array.isArray(b)) { return false; }
  if(!Object.keys(a) && !Object.keys(b)) { return true; }
  if(Object.keys(a).length !== Object.keys(b).length) { return false; }
  return Object.keys(a).reduce(function(bool, e, i, c) {
    if(bool === false) return bool;
    if(a[e] !== b[e]) bool = false;
    if(typeof a[e] === "object") return deepEquals(a[e], b[e]);
    return bool;
  }, true);
};
