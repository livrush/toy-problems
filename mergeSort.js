// {
// Merge Sort
// Implement a function that sorts an array of numbers using the “mergesort” algorithm.
//
// Mergesort uses a divide-and-conquer strategy. It begins by treating the input list of length N as a set of N “sublists” of length 1, which are considered to be sorted. Adjacent sublists are then “merged” into sorted sublists of length 2, which are merged into sorted sublists of length 4, and so on, until only a single sorted list remains. (Note, if N is odd, an extra sublist of length 1 will be left
// after the first merge, and so on.)
//
// This can be implemented using either a recursive (“top-down”) or an iterative (“bottom-up”) approach.}

function mergeSort(arr) {
  let separate = arr.map(val => [val]);

  const sortArray = function(array) {
    // let cache = {};
    let res = []
    let cache = array.reduce((cache, num) => {
      cache[num] ? cache[num] += 1 : cache[num] = 1;
      return cache;
    }, {});
    for (num in cache) {
      for (var i = 0; i < cache[num]; i++) {
        res.push(Number(num));
      }
    }
    return res;
  };

  const merge = function(array) {
    if(array.length === 1) {
      let res = sortArray(array[0]);
      return res;
    }
    let newArray = [];
    array.forEach(function(val, ind) {
      let next = array[ind + 1];
      if (!(ind % 2)) {
        if (next) {
          newArray.push([].concat(val).concat(next));
        } else {
          newArray[newArray.length - 1] = newArray[newArray.length - 1].concat(val);
        }
      }
    })
    let sorted = newArray.map(function(arr) {
      return sortArray(arr);
    })
    return merge(sorted);
  }

  return merge(separate);
};
