// {Balanced Brackets
// Given a string, return true if it contains all balanced parenthesis (), curly-brackets {}, and square-brackets [].
//
// I String O Boolean E Asymmetrical brackets C idk}

function isBalanced (str) {
  // Write your code here, and
  // return your final answer.
  str = str.replace(/[^\(\)\{\}\[\]]/g,'').split('');

  let order = []

  str.reduce(function(order, curr) {
    if(curr === '(') {
      order.push('p')
    } else if(curr === '{') {
      order.push('c')
    } else if(curr === '[') {
      order.push('b')
    } else if(curr === ')') {
      if(order[order.length - 1] !== 'p') {
        order.push('p')
      } else {
        order.pop();
      }
    } else if(curr === '}') {
      if(order[order.length - 1] !== 'c') {
        order.push('c')
      } else {
        order.pop();
      }
    } else if(curr === ']') {
      if(order[order.length - 1] !== 'b') {
        order.push('b')
      } else {
        order.pop();
      }
    }
    return order;
  }, order)

  return order.length ? false : true;
}
