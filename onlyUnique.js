function onlyUnique (str) {
  // Write your code here, and
  // return your final answer.
  let cache = {};
  str.split('').forEach(char => cache[char] ? cache[char]++ : cache[char] = 1);
  return str.split('').reduce((res, char) => cache[char] === 1 ? res += char : res, '');
}
