function BinaryHeap () {
  this._heap = [];
  // this compare function will result in a minHeap, use it to make comparisons between nodes in your solution
  this._compare = function (i, j) { return i < j };
}
// This function works just fine and shouldn't be modified
BinaryHeap.prototype.getRoot = function () {
  return this._heap[0];
}

BinaryHeap.prototype.insert = function (value) {
  // TODO: Your code here
  let difference;
  this._heap.length % 2 ? difference = 2 : difference = 1;
  let index = this._heap.length - difference;
  if (this._compare(value, this._heap[index])) {
    this._heap.unshift(value);
  } else {
    this._heap.push(value);
  }
}

BinaryHeap.prototype.removeRoot = function () {
  // TODO: Your code here
  let change = function(a, b) {
    if(this._heap[b] === undefined || this._heap[a] === undefined) {
      return null;
    };
    if(this._compare(this._heap[b], this._heap[a])) {
      const save = this._heap[a];
      this._heap[a] = this._heap[b];
      this._heap[b] = save;
    };
  }.bind(this);

  const length = this._heap.length;
  const result = this.getRoot();
  this._heap[0] = this._heap.pop();

  let parent = 0;
  let child = 1;
  let step = 0;
  while (parent < length) {
    change(parent, child);
    child++
    if(step % 2) {
      parent++;
    }
    step++;
  }
  return result;
}
