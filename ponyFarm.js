function getPonyAllergies (ponies, ownerEmail) {
  let allergies = ponies.reduce(function(foods, pony) {
    if (pony.email === ownerEmail) {
      foods = foods.concat(pony.allergies);
    }
    return foods;
  }, []).sort();
  allergies = allergies.reduce(function(obj, food) {
    if (!obj[food]) {
      obj[food] = food;
    }
    return obj;
  }, {});
  return Object.keys(allergies);
}

Array.prototype.concatAll = function() {
  return this.reduce(function (result, subArray) {
    return result.concat(subArray)
  })
}

function getProp(name) {
  return function (object) { return object[name] }
}
