// {Range Class
// Build a class to represent a range of numbers that has:
//
// a beginning index
// an end index (optional). If there is no end index, the range should include only the passed-in start value.
// a ‘step’ (optional)
// it should not store the range as an array of numbers; it should work in constant space.
// The step is the interval at which elements are included. For instance, a step of 1 includes every element in the range, while a step of 2 includes every other element.
//
// You should allow a negative value for ‘step’ to count backwards. If no step is provided and the start is more than the end, assume we’re counting backwards.
//
// The range should have a constructor that accepts these arguments in this order:
//
// beginning index
// end index
// step interval
// It should also support the following utility functions:
//
// size(): return the number of items represented by the range
// each(callback(index)): iterate over the range, passing each value to a callback function
// includes(index): return whether or not the range includes the passed value
// You should also be aware of the following caveats:
//
// Should return null if we are given no ‘start’ value.
// Again, Range should use constant space, even during the each() method, * i.e. you should not use an array as backing storage.
// USAGE EXAMPLES:
//
//  var myRange = new Range(0,10); // a new range representing the numbers between 0 and 10 (inclusively)
//
//  var evenNumbers = new Range(2,8,2); // A range with the even numbers 2, 4, 6, and 8.
//
//  evenNumbers.each(function(val){ console.log(val+'!'); }); //Prints '2! 4! 6! 8!'
//
//  evenNumbers.size() //4
//
//  evenNumbers.includes(2) //True
//
//  evenNumbers.include(3) //False}

var Range = function(start, end, step) {
  //Your code here
  if (!arguments) { return null; }
  let change = step || 1;
  if(!step && start > end) { change = -1; }
  let stop = end || Math.floor(start);
  if(end === 0) { stop = 0; }

  let makeNode = function(value) {
    let node = {};
    node.value = value;
    node.next = null;
    node.addNode = function(value) {
      if(!this.next) {
        this.next = makeNode(value);
      } else {
        this.next.addNode(value);
      }
    }
    return node;
  }

  this.head = makeNode(start);
  start += change;

  if (start < stop) {
    while(start <= stop) {
      this.head.addNode(start);
      start += change;
    }
  } else if (start > stop) {
    while(start > stop + change) {
      this.head.addNode(start);
      start += change;
    }
  }
};

Range.prototype.size = function () {
  //Your code here
  let size = 0;

  let counter = function(node) {
    size++;
    if(node.next) {
      return counter(node.next);
    }
  }

  counter(this.head);

  return size;
};

Range.prototype.each = function (callback) {
  //Your code here
  let manipulator = function(node) {
    callback(node.value);
    if(node.next) {
      return manipulator(node.next);
    }
  }

  manipulator(this.head);
};

Range.prototype.includes = function (val) {
  //Your code here
  let check = function(node) {
    if (node.value !== val && !node.next) {
      return false;
    } else if (node.value === val) {
      return true;
    }
    return check(node.next);
  }

  return check(this.head);
};
