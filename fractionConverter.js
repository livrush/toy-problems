function fractionConverter (number) {
  // Write your code here, and
  // return your final answer.
  let res = [0, 0];
  let num = number.toString().split('.');

  if(num.length === 1) {
    res[0] = num;
    res[1] = 1;
    return res.join('/');
  };

  if(num[1].length === 1) {
    num[1] *= 10;
  }

  let full = num.join('');

  if(!(num[1] % 25)) {
    let small = num[1] / (num[1] / 25);
    res[0] = full / small;
    res[1] = 4;
  } else {
    let max = '1';
    for(i = 0; i < num[1].length; i++) {
      max += '0';
    }
    res[0] = num[1];
    res[1] = max;
  }

  while(!(res[0] % 2) && !(res[1] % 2)) {
    if(res.includes(0)) {
      break;
    }
    res = res.map(function(e) {
      return e /= 2;
    })
  }

  return res.join('/')
}
