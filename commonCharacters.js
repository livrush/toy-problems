// COMMON CHARACTERS
// Write a function that accepts two strings as arguments, and returns only the characters that are common to both strings.
//
// Your function should return the common characters in the same order that they appear in the first argument. Do not return duplicate characters and ignore whitespace in your returned string.

// I - Two strings
// O - String of common characters
// E -
// C - Return in same order they appear in first argument
//
// Example: commonCharacters('acexivou', 'aegihobu')
//
// Returns: 'aeiou'

commonCharacters = function(string1, string2){
  let result = "";
  let cache = string1.split("").reduce(function(cache, e, i, a) {
    if(!cache[e] && e !== " ") cache[e] = 1;
    return cache;
  }, {});
  let cache2 = string2.split("").reduce(function(cache, e, i, a) {
    if(!cache[e] && e !== " ") cache[e] = 1;
    return cache;
  }, {});
  for (var k in cache2) { if (cache[k]) cache[k] = 2; }
  string1.split("").forEach(function(e, i, a) { if(cache[e] > 1 && result.indexOf(e) === -1) result += e })
  return result;
};
