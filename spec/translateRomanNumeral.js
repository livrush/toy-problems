/* global translateRomanNumeral, describe, it, expect, should */

describe('translateRomanNumeral()', function () {
  'use strict';

  it('should exist', function () {
    expect(translateRomanNumeral).to.be.a('function');

  });

  it('converts roman numerals', function () {
    const inp = 'LX';
    const res = 60;
    expect(translateRomanNumeral(inp)).to.equal(res);
  });

  it('converts roman numerals', function () {
    const inp = 'IV';
    const res = 4;
    expect(translateRomanNumeral(inp)).to.equal(res);
  });

  it('returns null for non-roman-numbers', function () {
    const inp = 'horse';
    const res = 'null';
    expect(translateRomanNumeral(inp)).to.equal(res);
  });

  it('returns 0 for empty strings', function () {
    const inp = '';
    const res = 0;
    expect(translateRomanNumeral(inp)).to.equal(res);
  });

  it('returns null for non-roman-numbers', function () {
    const inp = 'IaV';
    const res = 'null';
    expect(translateRomanNumeral(inp)).to.equal(res);
  });

  // Add more assertions here
});
