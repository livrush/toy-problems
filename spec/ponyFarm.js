/* global ponyFarm, describe, it, expect, should */

describe('ponyFarm()', function () {
  'use strict';

  const ponies = [
    { "id": 427, "name": "Firefly", "allergies": [ "gluten", "peanut" ], "email": "cindy@ponymail.com" },
    { "id": 23, "name": "Black Lightning", "allergies": [ "soy", "peanut" ], "email": "sandy@hotmail.com" },
    { "id": 458, "name": "Picadilly", "allergies": [ "corn", "gluten" ], "email": "cindy@ponymail.com" },
    { "id": 142, "name": "Brad", "allergies": [ "gluten", "chicken" ], "email": "sandy@hotmail.com" },
    { "id": 184, "name": "Cahoot", "allergies": [ "soy", "peanut", "gluten" ], "email": "jimmy@ponymail.com" }
  ];

  it('exists', function () {
    expect(getPonyAllergies).to.be.a('function');

  });

  it('does something', function () {
    const ownerEmail = "sandy@hotmail.com";
    const result = [ "chicken", "gluten", "peanut", "soy" ];
    expect(getPonyAllergies(ponies, ownerEmail)).to.eql(result);
  });

});
