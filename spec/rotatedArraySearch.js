/* global rotatedArraySearch, describe, it, expect, should */

describe('rotatedArraySearch()', function () {
  'use strict';

  it('exists', function () {
    expect(rotatedArraySearch).to.be.a('function');

  });

  it('should produce correct index of values in the array', function () {
    let test = [ 4, 5, 6, 0, 1, 2, 3 ];

    expect(rotatedArraySearch(test, 2)).to.equal(5);
  });

  it('should produce correct index when only one value in array', function () {
    let test = [ 1 ];

    expect(rotatedArraySearch(test, 1)).to.equal(0);
  });

  it('should produce -1 for values not in the array', function () {
    let test = [ 4, 5, 6, 0, 1, 2, 3 ];

    expect(rotatedArraySearch(test, 100)).to.equal(-1);
  });

  // Add more assertions here
});
