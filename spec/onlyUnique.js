/* global onlyUnique, describe, it, expect, should */

describe('onlyUnique()', function () {
  'use strict';

  it('should exist', function () {
    expect(onlyUnique).to.be.a('function');
  });

  it('should return a string', function () {
    expect(onlyUnique('')).to.be.a('string');
  });

  it('should return only unique values', function () {
    const str = "abccdefe";
    const res = "abdf";
    expect(onlyUnique(str)).to.equal(res);
  });

  it('should return only unique values', function () {
    const str = "hello there";
    const res = "o tr";
    expect(onlyUnique(str)).to.equal(res);
  });

  it('should return only unique values', function () {
    const str = "xyz";
    const res = "xyz";
    expect(onlyUnique(str)).to.equal(res);
  });

  it('should return only unique values', function () {

    expect(onlyUnique(str)).to.equal(res);
  });
    const str = "iiii";
    const res = "";
  // Add more assertions here
});
