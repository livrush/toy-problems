/* global isBalanced, describe, it, expect, should */

describe('isBalanced()', function () {
  'use strict';

  it('should be a function', function () {
    expect(isBalanced).to.be.a('function');

  });

  it('should return a boolean', function () {
    expect(isBalanced('')).to.be.a('boolean');
  });

  it('should return true for correctly balanced string', function () {
    let str = "(x + y) - (4)"
    expect(isBalanced(str)).to.equal(true);
  });

  it('should return true for correctly balanced string', function () {
    let str = "(((10 ) ()) ((?)(:)))"
    expect(isBalanced(str)).to.equal(true);
  });

  it('should return true for correctly balanced string', function () {
    let str = "[{()}]"
    expect(isBalanced(str)).to.equal(true);
  });

  it('should return false for incorrectly balanced string', function () {
    let str = ")("
    expect(isBalanced(str)).to.equal(false);
  });

  it('should return false for incorrectly balanced string', function () {
    let str = "(50)("
    expect(isBalanced(str)).to.equal(false);
  });

  it('should return false for incorrectly balanced string', function () {
    let str = "[{]}"
    expect(isBalanced(str)).to.equal(false);
  });

  it('should return false for incorrectly balanced string', function () {
    let str = "[]]"
    expect(isBalanced(str)).to.equal(false);
  });

  // Add more assertions here
});
