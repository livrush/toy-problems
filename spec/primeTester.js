/* global primeTester, describe, it, expect, should */

describe('primeTester()', function () {
  'use strict';

  let n, result;

  it('exists', function () {
    expect(primeTester).to.be.a('function');

  });

  it('finds prime numbers', function () {
    n = 1;
    result = primeTester(n);
    expect(result).to.equal(false);
  });

  it('finds prime numbers', function () {
    n = 2;
    result = primeTester(n);
    expect(result).to.equal(true);
  });

  it('finds prime numbers', function () {
    n = 4;
    result = primeTester(n);
    expect(result).to.equal(false);
  });

  it('finds prime numbers', function () {
    n = 91;
    result = primeTester(n);
    expect(result).to.equal(false);
  });

  it('finds prime numbers', function () {
    n = 1147;
    result = primeTester(n);
    expect(result).to.equal(false);
  });

  it('finds prime numbers', function () {
    n = 8828119010022395000;
    result = primeTester(n);
    expect(result).to.equal(false);
  });

  it('finds prime numbers', function () {
    n = 15485867;
    result = primeTester(n);
    expect(result).to.equal(true);
  });

  it('finds prime numbers', function () {
    n = 239812076741689;
    result = primeTester(n);
    expect(result).to.equal(false);
  });

  it('finds prime numbers', function () {
    n = 22499;
    result = primeTester(n);
    expect(result).to.equal(false);
  });

  // Add more assertions here
});
