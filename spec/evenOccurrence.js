/* global evenOccurrence, describe, it, expect, should */

describe('evenOccurrence()', function () {
  'use strict';

  it('exists', function () {
    expect(evenOccurrence).to.be.a('function');

  });

  it('finds first even occuring value', function () {
    const data = [ 1, 3, 3, 3, 2, 4, 4, 2, 5 ];
    const res = evenOccurrence(data);
    expect(res).to.equal(2);
  });

  it('finds first even occuring value', function () {
    const data = [ "cat", "dog", "dig", "cat" ];
    const res = evenOccurrence(data);
    expect(res).to.equal('cat');
  });

  it('finds first even occuring value', function () {
    const data = ["rob","victor","fred","jess","rob","rob"];
    const res = evenOccurrence(data);
    expect(res).to.equal(null);
  });

  // Add more assertions here
});
