/* global largestProductOfThree, describe, it, expect, should */

describe('largestProductOfThree()', function () {
  'use strict';

  it('exists', function () {
    expect(largestProductOfThree).to.be.a('function');

  });

  it('find largest product of n numbers', function () {
    let array = [ 2, 1, 3, 7 ];
    expect(largestProductOfThree(array)).to.equal(42);
  });

  it('find correct answer when only three numbers in array', function () {
    let array = [ 0, 2, 3 ];
    expect(largestProductOfThree(array)).to.equal(0);
  });

  it('correctly deal with negative numbers', function () {
    let array = [-31, 41, 34, -37, -17, 29];
    expect(largestProductOfThree(array)).to.equal(47027);
  });

  it('correctly deal with negative numbers', function () {
    let array = [2, 11, 13, 7, 13, 3, 11, 5];
    expect(largestProductOfThree(array)).to.equal(1859);
  });

  // Add more assertions here
});
