/* global powerSet, describe, it, expect, should */

describe('powerSet()', function () {
  'use strict';

  it('powerSet is a function', function () {
    expect(powerSet).to.be.a('function');
  });

  it('powerSet returns an array', function () {
    expect(Array.isArray(powerSet('yo'))).to.equal(true);
  });

  it('should produce correct result', function () {
    let string = 'a';
    let result = [ "", "a" ];
    expect(powerSet(string)).to.eql(result);
  });

  it('should produce correct result', function () {
    let string = 'ab';
    let result = [ "", "a", "ab", "b" ];
    expect(powerSet(string)).to.eql(result);
  });

  it('should produce correct result', function () {
    let string = 'horse';
    let result = [ "", "e", "eh", "eho", "ehor", "ehors", "ehos", "ehr", "ehrs", "ehs", "eo", "eor", "eors", "eos", "er", "ers", "es", "h", "ho", "hor", "hors", "hos", "hr", "hrs", "hs", "o", "or", "ors", "os", "r", "rs", "s" ];
    expect(powerSet(string)).to.eql(result);
  });

  it('should produce correct result', function () {
    let string = 'obama';
    let result = [ "", "a", "ab", "abm", "abmo", "abo", "am", "amo", "ao", "b", "bm", "bmo", "bo", "m", "mo", "o" ]
    expect(powerSet(string)).to.eql(result);
  });
});
