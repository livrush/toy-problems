/* global binarySearch, describe, it, expect, should */

describe('binarySearch()', function () {
  'use strict';

  it('exists', function () {
    expect(binarySearch).to.be.a('function');

  });

  it('returns -1 for number not found in array', function () {
    let array = [5];
    let target = 4;
    expect(binarySearch(array, target)).to.equal(-1);
  });

  it('returns -1 for number not found in array', function () {
    let array = [ 11, 12, 13, 14, 15 ];
    let target = 16;
    expect(binarySearch(array, target)).to.equal(-1);
  });

  it('finds index number in array below middle', function () {
    let array = [ 11, 12, 13, 14, 15 ];
    let target = 11;
    expect(binarySearch(array, target)).to.equal(0);
  });

  it('finds index number in array below middle', function () {
    let array = [ 11, 12, 13, 14, 15 ];
    let target = 12;
    expect(binarySearch(array, target)).to.equal(1);
  });

  it('finds index number in array at middle', function () {
    let array = [ 11, 12, 13, 14, 15 ];
    let target = 13;
    expect(binarySearch(array, target)).to.equal(2);
  });

  it('finds index number in array above middle', function () {
    let array = [ 11, 12, 13, 14, 15 ];
    let target = 14;
    expect(binarySearch(array, target)).to.equal(3);
  });

  it('finds index number in array above middle', function () {
    let array = [ 11, 12, 13, 14, 15 ];
    let target = 15;
    expect(binarySearch(array, target)).to.equal(4);
  });

  it('finds index number in array with an even number of values below middle', function () {
    let array = [0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,4700,4800,4900];
    let target = 1700;
    expect(binarySearch(array, target)).to.equal(17);
  });

  it('finds index number in array with an even number of values above middle', function () {
    let array = [1,22,33,45,53,62];
    let target = 45;
    expect(binarySearch(array, target)).to.equal(3);
  });

  it('finds index number in array with an even number of values below middle', function () {
    let array = [1,22,33,45,53,62];
    let target = 33;
    expect(binarySearch(array, target)).to.equal(2);
  });

});
