/* global nonRepeatedCharacters, describe, it, expect, should */

describe('nonRepeatedCharacters', function () {
  'use strict';

  it('firstNonRepeatedCharacter should be a function', function () {
    expect(firstNonRepeatedCharacter).to.be.a('function');
  });

  it('should produce "A" when passed "ABCDBIRDUP"', function () {
    let result = firstNonRepeatedCharacter("ABCBIRDUP")
    expect(result).to.equal("A");
  });

  it('should produce "sorry" when passed "XXXXXXX"', function () {
    let result = firstNonRepeatedCharacter("XXXXXXX")
    expect(result).to.equal("sorry");
  });

  it('should produce "A" when passed "ALABAMA"', function () {
    let result = firstNonRepeatedCharacter("ALABAMA")
    expect(result).to.equal("L");
  });

  it('should produce "sorry" when passed "BABA"', function () {
    let result = firstNonRepeatedCharacter("BABA")
    expect(result).to.equal("sorry");
  });

});
