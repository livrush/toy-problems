/* global fractionConverter, describe, it, expect, should */

describe('fractionConverter()', function () {
  'use strict';

  it('should be a function', function () {
    expect(fractionConverter).to.be.a('function');
  });

  it('should return a string', function () {
    expect(fractionConverter(1)).to.be.a('string');
  });

  it('should convert 1 correctly', function () {
    let input = 1;
    let res = "1/1";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 0.25 correctly', function () {
    let input = 0.25;
    let res = "1/4";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 0.5 correctly', function () {
    let input = 0.5;
    let res = "1/2";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 3 correctly', function () {
    let input = 3;
    let res = "3/1";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 2.5 correctly', function () {
    let input = 2.5;
    let res = "5/2";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 2.75 correctly', function () {
    let input = 2.75;
    let res = "11/4";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 0.5 correctly', function () {
    let input = 0.5;
    let res = "1/2";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 0.88 correctly', function () {
    let input = 0.88;
    let res = "22/25";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 0 correctly', function () {
    let input = 0;
    let res = "0/1";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 82 correctly', function () {
    let input = 82;
    let res = "82/1";
    expect(fractionConverter(input)).to.equal(res);
  });

  it('should convert 0.253213 correctly', function () {
    let input = 0.253213;
    let res = "253213/1000000";
    expect(fractionConverter(input)).to.equal(res);
  });


  // Add more assertions here
});


// [0.253213],[0],[1.75],[0.5],[-1.75],[2.5],[-1],[82]
