/* global countIslands, describe, it, expect, should */

describe('countIslands()', function () {
  'use strict';

  it('exists', function () {
    expect(countIslands).to.be.a('function');

  });

  it('should find two islands', function () {
    let island = ".0...\n.00..\n....0"
    expect(countIslands(island)).to.equal(2);
  });

  it('should find three islands', function () {
    let island = "..000.\n..000.\n..000.\n.0....\n..000."
    expect(countIslands(island)).to.equal(3);
  });

  it('should find two islands', function () {
    let island = "..000.\n..0...\n..0.0.\n..0...\n..000."
    expect(countIslands(island)).to.equal(2);
  });

  it('should find five islands', function () {
    let island = "0...0\n..0..\n0...0"
    expect(countIslands(island)).to.equal(5);
  });

  it('should find one island', function () {
    let island = "0...0\n0...0\n00000"
    expect(countIslands(island)).to.equal(1);
  });
});
