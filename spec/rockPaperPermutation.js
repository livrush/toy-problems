/* global rockPaperPermutation, describe, it, expect, should */

describe('rockPaperPermutation', function () {
  'use strict';

  it('rockPaperPermutation should be a function', function () {
    expect(rockPaperPermutation).to.be.a('function');
  });

  it('should return correct results when roundCount is 0', function () {
    const expected = [];
    let actual = rockPaperPermutation(0)
    expect(actual).to.deep.equal(expected);
  });

  it('should return correct results when roundCount is 1', function () {
    const expected = ["r", "p", "s"];
    let actual = rockPaperPermutation(1);
    expect(actual).to.deep.equal(expected);
  });

  it('should return correct results when roundCount is 2', function () {
    const expected = [ "rr", "rp", "rs", "pr", "pp", "ps", "sr", "sp", "ss" ];
    let actual = rockPaperPermutation(2);
    expect(actual).to.deep.equal(expected);
  });

  it('should return correct results when roundCount is 3', function () {
    const expected = ["rrr","rrp","rrs","rpr","rpp","rps","rsr","rsp","rss","prr","prp","prs","ppr","ppp","pps","psr","psp","pss","srr","srp","srs","spr","spp","sps","ssr","ssp","sss"];
    let actual = rockPaperPermutation(3);
    expect(actual).to.deep.equal(expected);
  });

});
