/* global rotateMatrix, describe, it, expect, should */

describe('rotateMatrix()', function () {
  'use strict';

  it('should exist', function () {
    expect(rotateMatrix).to.be.a('function');

  });

  it('should rotate matrix correctly', function () {
    let input = [ [ 1, 2, 3, 4 ], [ 5, 6, 7, 8 ], [ 9, "A", "B", "C" ], [ "D", "E", "F", "G" ] ]
    let result = [ [ "D", 9, 5, 1 ], [ "E", "A", 6, 2 ], [ "F", "B", 7, 3 ], [ "G", "C", 8, 4 ] ]
    expect(rotateMatrix(input)).to.eql(result);
  });

  it('should rotate matrix correctly', function () {
    let input = [ [ 1 ] ]
    let result = [ [ 1 ] ]
    expect(rotateMatrix(input)).to.eql(result);
  });

  it('should rotate matrix correctly', function () {
    let input = [ [ 1, 2 ], [ 3, 4 ] ]
    let result = [ [ 3, 1 ], [ 4, 2 ] ]
    expect(rotateMatrix(input)).to.eql(result);
  });

  it('should rotate matrix correctly', function () {
    let input = [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ] ]
    let result = [ [ 7, 4, 1 ], [ 8, 5, 2 ], [ 9, 6, 3 ] ]
    expect(rotateMatrix(input)).to.eql(result);
  });

  // Add more assertions here
});
