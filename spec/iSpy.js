/* global iSpy, describe, it, expect, should */

describe('iSpy()', function () {
  'use strict';

  it('exists', function () {
    expect(iSpy).to.be.a('function');

  });

  it('does something', function () {
    expect(true).to.equal(false);
  });

  it('does something else', function () {
    expect(true).to.equal(false);
  });

  // Add more assertions here
});
