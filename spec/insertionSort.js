/* global insertionSort, describe, it, expect, should */

describe('insertionSort()', function () {
  'use strict';

  let array, sorted, result;

  it('exists', function () {
    expect(insertionSort).to.be.a('function');
  });

  it('sorts array of objects with values', function () {
    array = [ { "value": 3 }, { "value": 1 }, { "value": 2 } ]	;
    sorted = [ { "value": 1 }, { "value": 2 }, { "value": 3 } ];
    result = insertionSort(array);
    expect(result).to.eql(sorted);
  });

  it('sorts array of objects with values and orders', function () {
    array = [ { "value": 10 }, { "value": 5, "order": 1 }, { "value": 5, "order": 2 } ];
    sorted = [ { "value": 5, "order": 1 }, { "value": 5, "order": 2 }, { "value": 10 } ];
    result = insertionSort(array);
    expect(result).to.eql(sorted);
  });

  it('sorts array of objects with values and orders', function () {
    array = [{"value":10},{"value":5,"order":1},{"value":2},{"value":5,"order":2},{"value":1}];
    sorted = [{"value":1},{"value":2},{"value":5,"order":1},{"value":5,"order":2},{"value":10}];
    result = insertionSort(array);
    expect(result).to.eql(sorted);
  });

  // Add more assertions here
});
