/* global robotPaths, describe, it, expect, should */

describe('robotPaths()', function () {
  'use strict';

  it('exists', function () {
    expect(robotPaths).to.be.a('function');

  });

  it('finds all possible unique paths for 1 x 1 board', function () {
    let test = robotPaths(1)
    let result = 1
    expect(test).to.equal(result);
  });

  it('finds all possible unique paths for 2 x 2 board', function () {
    let test = robotPaths(2)
    let result = 2
    expect(test).to.equal(result);
  });

  it('finds all possible unique paths for 3 x 3 board', function () {
    let test = robotPaths(3)
    let result = 12
    expect(test).to.equal(result);
  });

  it('finds all possible unique paths for 5 x 5 board', function () {
    let test = robotPaths(5)
    let result = 8512
    expect(test).to.equal(result);
  });
  // Add more assertions here
});
