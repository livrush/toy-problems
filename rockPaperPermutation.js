function rockPaperPermutation (roundCount) {
  let result = arguments[1] || [];
  let possible = ["r", "p", "s"];
  if(roundCount === 0) { return result; };
  if(result.length === 0) {
    possible.forEach(function(e, i, a) {
      for(var n = Math.pow(3, roundCount) / 3; n > 0; n--) {
        result.push(e);
      }
    })
  } else {
    let n = Math.pow(3, roundCount) / 3;
    var i = 0;
    result.forEach(function(v, k, c) {
      result[k] = v + possible[i];
      n--;
      if(n === 0) {
        n = Math.pow(3, roundCount) / 3;
        i++;
        if(i === 3) {
          i = 0;
        }
      }
    })
  }
  return rockPaperPermutation(roundCount - 1, result);
}

// console.log(rockPaperPermutation(2))
// console.log(rockPaperPermutation(4))

// ["rrr","rrp","rrs","rpr","rpp","rps","rsr","rsp","rss","prr","prp","prs","ppr","ppp","pps","psr","psp","pss","srr","srp","srs","spr","spp","sps","ssr","ssp","sss"]
// ["rrr","rpp","rss","rrr","rpp","rss","rrr","rpp","rss","prr","ppp","pss","prr","ppp","pss","prr","ppp","pss","srr","spp","sss","srr","spp","sss","srr","spp","sss"]
