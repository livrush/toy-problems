// I - string, O - number, C - idk, E - when something connects at the bottom

function countIslands(mapStr) {
  let count = 0;
  const arr = mapStr.split("\n");
  let islandView = arr.map(row => row.split(''));
  const rows = islandView.length  - 1;
  const columns = islandView[0].length - 1;

  const checkSurroundingForIsland = function(row, column) {
    islandView[row][column] = '.';
    if(row > 0) {
      if (islandView[row - 1][column] === '0') {
        checkSurroundingForIsland(row - 1, column);
      }
    }
    if(column > 0) {
      if (islandView[row][column - 1] === '0') {
        checkSurroundingForIsland(row, column - 1);
      }
    }
    if(row < rows) {
      if (islandView[row + 1][column] === '0') {
        checkSurroundingForIsland(row + 1, column);
      }
    }
    if(column < columns) {
      if (islandView[row][column + 1] === '0') {
        checkSurroundingForIsland(row, column + 1);
      }
    }
  }

  const checkForIsland = function(row, column) {
    if(islandView[row][column] === '0') {
      count++;
      checkSurroundingForIsland(row, column);
    }
  }

  islandView.forEach(function(row, r, a) {
    row.forEach(function(column, c, a) {
      checkForIsland(r, c);
    })
  });

  return count;
}

// Check if the current value is true or false
  // If false, continue to next value
  // If true, add 1 to count
    // Set the value to false so it won't be counted again
    // Then run helper function to check neighboring values
      // If one is true, run the countIslands function on it (?)
      // This should go sequentially through all connected island pieces, changing them to false

// console.log(islandView);

// function countIslands (mapStr) {
//   // Write your code here, and
//   // return your final answer.
//   let count = 0;
//   let islandView = mapStr.split("\n");
//   // console.log(islandView)
//   islandView.forEach(function(island, index, collection) {
//     island.split("").forEach(function(e, i, a) {
//       if (e === "0") {
//         if(index === 0) {
//           if(islandView[index][i - 1] !== "0") {
//             count += 1;
//           }
//         } else if (index === islandView.length - 1) {
//           if(islandView[index][i - 1] !== "0") {
//             if(islandView[index - 1][i] === ".") {
//               count += 1;
//             }
//           }
//         } else {
//           if(islandView[index][i - 1] !== "0") {
//             if(islandView[index - 1][i] === "." && islandView[index + 1][i] === ".") {
//               count += 1;
//             }
//           }
//         }
//       }
//     })
//   })
//   return count;
// }
