function robotPaths (n) {
  // Write your code here, and
  // return your final answer.
  let routeCount = 0;
  const board = makeBoard(n);
  board[n - 1][n - 1] = 'end';
  console.log(board);

  console.log(Object.create(board));

  const traveler = function(matrix, x, y) {
    console.log(n, '|', x, y);
    console.log(matrix === board)
    let b = matrix.slice();
    if (b[y][x] === 'end') {
      console.log('end');
      routeCount++;
    } else {
      b[y][x] = !b[y][x];
      if (b[y][x + 1] === false || b[y][x + 1] === 'end') {
        console.log('>');
        traveler(b, x + 1, y);
      }
      if (b[y + 1]) {
        if (b[y + 1][x] === false || b[y + 1][x] === 'end') {
          console.log('v');
          traveler(b, x, y  + 1);
        }
      }
      if (b[y][x - 1] === false) {
        console.log('<');
        traveler(b, x - 1, y);
      }
      if (b[y - 1]) {
        if (b[y - 1][x] === false) {
          console.log('^');
          traveler(b, x, y  - 1);
        }
      }
    }
  }

  traveler(board, 0, 0);
  return routeCount;
}

function makeBoard(n) {
  var board = [];
  for (var i = 0; i < n; i++) {
    board.push([]);
    for (var j = 0; j < n; j++) {
      board[i].push(false);
    }
  }
  board.togglePiece = function(i, j) {
    this[i][j] = !this[i][j];
  }
  board.hasBeenVisited = function(i, j) {
    return !!this[i][j];
  }
  return board;
}
