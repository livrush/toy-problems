// I - Array of n numbers
// O - Number
// C -
// E - Negative numbers

// function largestProductOfThree (array) {
//   let result = array.sort().reverse().slice(0, 3);
//   console.log(result);
//   return result.reduce(function(product, value) {
//     console.log("before", product)
//     product *= value;
//     console.log("before", product)
//     return product
//   })
// }

function largestProductOfThree (array) {
  function sortNumber(a, b) { return a - b; }
  function product(array) { return array.reduce((product, value) => product * value); }
  if(array.length === 3) { return product(array); };
  let pos = array.sort(sortNumber).reverse().slice(0, 3);
  let neg = array.sort(sortNumber).slice(0, 2);
  neg.push(pos[0]);
  return product(pos) > product(neg) ? product(pos) : product(neg);
}
