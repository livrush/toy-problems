function rotateMatrix (matrix) {
  // Write your code here, and
  // return your final answer.
  // Take the last element from each array, put into new array
  // let result = []

  // for (var i = 0; i < matrix.length; i++) {
  //   let arr = matrix.reduceRight(function(res, e, ind, a) {
  //     console.log(e)
  //     res.push(e[i]);
  //     return res;
  //   }, []);
  //   result.push(arr);
  // }

  let arrayMaker = function(array) {
    let result = [];
    if(!array[0].length) {
      return result;
    }
    arr = array.reduceRight((res, arr, ind, a) => res.concat([arr[0]]), []);
    array = array.map(e => e.slice(1));
    result.push(arr);
    return result.concat(arrayMaker(array));
  }

  let result = arrayMaker(matrix, 0);

  console.log(result);
  return result;
}
