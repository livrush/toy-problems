const linkedListIntersection = (list1,list2) => {
  //Your beautiful code here
  let listA = [];
  let listB = {};
  const split = function(list, check) {
    if (check) {
      listA.push(list.value);
    } else {
      listB[list.value] = list.value;
    }
    if (list.next) {
      split(list.next, check);
    }
  }
  split(list1, true);
  split(list2, false);
  return listA.reduce(function(res, value) {
    if (!res && listB[value]) {
      const check = function(list) {
        if (list.value === value) {
          return list;
        }
        return check(list.next);
      }
      res = check(list1);
    }
    return res;
  }, null);
}

function Node (val) {
  var obj = {};
  obj.value = val || null;
  obj.next = null;
  return obj;
}
