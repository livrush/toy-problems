function evenOccurrence (arr) {
  // Write your code here, and
  // return your final answer.
  const cache = arr.reduce(function(obj, val) {
    obj[val] ? obj[val]++ : obj[val] = 1;
    return obj;
  }, {});
  for (var key in cache) {
    if (cache[key] % 2) {
      delete cache[key];
    }
  }
  return arr.filter(function(val) {
    return cache[val];
  })[0] || null;
};
