// Bubble Sort
// Bubble sort is considered the most basic sorting algorithm in Computer Science. It works by starting at the first element of an array and comparing it to the second element:
//
// If the first element is greater than the second element, it swaps the two.
// It then compares the second to the third, and the third to the fourth, and so on.
// In this way, the largest values ‘bubble’ to the end of the array.
// Once it gets to the end of the array, it starts over and repeats the process until the array is sorted numerically.
// Implement a function that takes an array and sorts it using this technique.
//
// NOTE: DO NOT use JavaScript’s built-in sorting function (Array.prototype.sort).

// I - Array
// O - Array sorted from lowest to highest
// E -
// C - No javascript sort function

// Create new array from argument
// Loop through array
  // Loop through the array again
  // If the current position is greater than the next
    // Replace them
  // If the current position is less than the next
    // Move to next in outer loop
  // Repeat until it gets to a well sorted array

var bubbleSort = function(array) {
  let result = array.slice(0);
  return result.reduce(function(array, e, i, a) {
    return result.reduce(function(result, v, k, c) {
      if(v > c[k + 1]) {
        let less = c[k + 1];
        let more = v;
        result[k + 1] = more;
        result[k] = less;
      }
      return result;
    }, result)
  }, result)
};
