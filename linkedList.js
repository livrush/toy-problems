var LinkedList = function (initialValue) {
// Write your code here
  initialValue ? this.head = node(initialValue) : this.head = null;
  this.tail = node(initialValue);
  this.head ? this.count = 1 : this.count = 0;
};

LinkedList.prototype.addToTail = function(value) {
  const newTail = node(value);
  if (!this.head) {
    this.head = node(value);
  }
  if (this.count === 1) {
    this.head.next = newTail;
  }
  this.tail.next = newTail;
  this.tail = newTail;
  this.count++;
};

LinkedList.prototype.removeHead = function() {
  if (this.count === 1) {
    this.head = null;
    this.tail = null;
  } else {
    this.head = this.head.next;
  }
  this.count--;
};

LinkedList.prototype.contains = function(target) {
  const search = (node) => {
    if (node.value === target) {
      return true;
    } else if (node.next === null) {
      return false;
    } else {
      return search(node.next);
    }
  }
  return search(this.head);
};

const node = value => {
  return {
    value: value,
    next: null,
  };
};
