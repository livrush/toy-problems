var Stack = function() {
  var storage = [];

  this.push = function(value) {
    storage.push(value);
  };

  this.pop = function() {
    return storage.pop();
  };

  this.size = function() {
    return storage.length;
  };

  this.storage = function() {
    return storage;
  }
};

var Queue = function() {

  var inbox = new Stack();
  var outbox = new Stack();

  this.enqueue = function(val) {
    inbox.push(val);
  };

  this.dequeue = function() {
    while (inbox.size() > 0) {
      outbox.push(inbox.pop());
    }
    let res = outbox.pop();
    while (outbox.size()) {
      inbox.push(outbox.pop());
    }
    return res;
  };

  this.size = function() {
    return inbox.size();
  };
};
