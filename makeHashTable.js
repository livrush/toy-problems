var makeHashTable = function(){
  var table = {};
  var storage = [];
  var storageLimit = 1000;

  table.insert = function(key, value){
    var index = getIndexBelowMaxForKey(key, storageLimit);
    storage[index] = storage[index] || [];
    //YOUR CODE HERE
    if (storage[index].length) {
      let stored = storage[index].reduce(function(res, obj) {
        if(key in obj) {
          obj[key] = value;
          res = true;
        }
        return res;
      }, false);
      if(!stored) {
        storage[index].push({ [key]: value });
      }
    } else {
      storage[index].push({ [key]: value });
    }
  };

  table.retrieve = function(key){
    //YOUR CODE HERE
    var index = getIndexBelowMaxForKey(key, storageLimit);
    if (!storage[index] || !storage[index].length) {
      return undefined;
    }
    // console.log(key, storage[index]);
    return storage[index].reduce((res, obj) => {
      if (key in obj) {
        res = obj[key];
      }
      return res;
    }, undefined);
  };

  table.remove = function(key){
    //YOUR CODE HERE
    let value;
    var index = getIndexBelowMaxForKey(key, storageLimit);
    storage[index].forEach((obj, ind) => {
      console.log(obj, key);
      if (key in obj) {
        storage[index].splice(ind, 1);
      }
    });
  }
  return table;
};

var getIndexBelowMaxForKey = function(str, max){
 var hash = 0;
 for (var i = 0; i < str.length; i++) {
   hash = (hash<<5) + hash + str.charCodeAt(i);
   hash = hash & hash; // Convert to 32bit integer
   hash = Math.abs(hash);
 }
 return hash % max;
};
