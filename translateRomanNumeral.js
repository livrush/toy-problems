const DIGIT_VALUES = {
  I: 1,
  V: 5,
  X: 10,
  L: 50,
  C: 100,
  D: 500,
  M: 1000
};

function translateRomanNumeral(str) {
  if (!str.length) {
    return 0;
  }
  let split = str.split('');
  return split.reduce((res, letter, index) => {
    if (DIGIT_VALUES[letter] && res !== 'null') {
      DIGIT_VALUES[split[index + 1]] > DIGIT_VALUES[letter] ?
      res -= DIGIT_VALUES[letter] :
      res += DIGIT_VALUES[letter];
    } else {
      res = 'null';
    }
    return res;
  }, 0);
}
