// nthFibonacci
// Suppose a newly-born pair of iguanas, one male, one female, are put in a large aquarium.
//
// Iguanas are able to mate at the age of one month so that at the end of its second month a female can produce another pair of iguanas.
//
// Suppose that our iguanas never die and that the female always produces one new pair (one male, one female) every month from the second month on.
//
// How many pairs of iguanas will there be after n months?
//
// For example, the iguana pair size for months zero through five are:
//
// 0 1 1 2 3 5
// If n were 4, your function should return 3; for 5, it should return 5.
//
// HINT: This iguana pattern is described exactly by the fibonacci sequence:
//
// https://en.wikipedia.org/wiki/Fibonacci_number
//
// Write a function that accepts a number n and returns the number of iguana pairs after n months.
//
// DO NOT use a recursive solution to this problem. Your solution must run in linear time.
//
// Note: Your solution may fail if you have a comment in your nthFibonacci function that contains the string literal “nthFibonacci” somewhere within it.

// I. Number n
// O. nth Fibonacci
// E. ~
// C. No recursion

// So we have a number
// From that number we want to create a new pair
// So we need to work backwards
// Create two variables, one for the current, and one for the last
// Make an inner function that loops through the thing until

nthFibonacci = function(n) {
let current = 0;
let next = 1;
let additive = current + next;
for (var i = n; i > 0; i--) {
  current = next;
  next = additive;
  additive = next + current;
}
return current;
};

// console.log(nthFibonacci(3))
