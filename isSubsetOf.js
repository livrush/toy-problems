// Make an array method that can return whether or not a context array is a subset of an input array. To simplify the problem, you can assume that both arrays will contain only strings.
// Input - Array
// Output - Boolean
// Edges -
// Constraints - arrays only contain strings

Array.prototype.isSubsetOf = function(array) {
  let checkObj = {};
  this.forEach((e, i, a) => checkObj[e] = 0);
  array.forEach((e, i, a) => checkObj[e] >= 0 ? checkObj[e]++ : null)
  return Object.values(checkObj).reduce((result, value) => value === 0 ? result = false : result, true)
}

// return Object.values(checkObj).reduce(function(result, value) {
//   if(value === 0) result = false;
//   return result;
//   console.log(result)
// }, true)
