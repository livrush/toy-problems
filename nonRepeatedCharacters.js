// NON-REPEATED CHARACTERS
// Given an arbitrary input string, return the first non-repeating character. For strings with all repeats, return 'sorry'.

// Examples
// Input	    Output
// string:   "ABCDBIRDUP"	"A"
// string:   "XXXXXXX"	"sorry"
// string:   "ALAMABA"	"L"
// string:   "BABA"	"sorry"

let firstNonRepeatedCharacter = function (string) {
  // Write your code here, and
  // return your final answer.
  let arr = string.split('');
  let result = [];
  let cache = {};
  arr.forEach(function(e, i, a) {
    if(!cache[e]) { cache[e] = 1; }
    else { cache[e]++; }
  });
  for(key in cache) {
    if(cache[key] > 1) { delete cache[key]; }
    else { result.push(key) }
  };
  if(!result.length) { return 'sorry'; };
  return result[0];
}
