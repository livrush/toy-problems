// {Binary Search Array
// Given a sorted array of integers, find the index of a target value using a binary search algorithm.
//
// A binary search finds an item in a sorted array by repeatedly choosing a middle value and dividing the search interval in half. The initial interval includes the whole array. If the value of the target value is less than the middle value of the interval, then the next interval will be the lower half of the current interval. If the value of the target value is greater than the middle value, then the next interval will be the upper half. The search process repeats until the middle value is equal to the target value, or the search interval is empty.
//
// Note:
// Your function should return -1 for target values not in the array.
//
// Do NOT use Array.prototype.indexOf in your solution. What would be the fun in that?
//
// I - Array, number
// O - Index number, or -1
// E -
// C - No indexOf}

// function binarySearch (array, target) {
//   let result;
//
//   const innerFunction = function(array, target) {
//     let round = arguments[2] || 1;
//
//     let middle = Math.floor(array.length / 2);
//
//     if (target === array[middle]) {
//       return middle;
//     }
//
//     if(array.length === 1 && target !== array[middle]) {
//       return -round || -1;
//     }
//
//     if (target > array[middle]) {
//       return middle + binarySearch(array.slice(middle), target, round + 1);
//     }
//
//     if (target < array[middle]) {
//       return binarySearch(array.slice(0, middle), target, round);
//     }
//   }
// }

function binarySearch(array, target) {
  let result = 0;
  if(target > array[array.length - 1] || target < array[0]) {
    return -1;
  }

  const innerFunction = function(array, target) {
    let middle = parseInt(array.length / 2);

    if (target === array[middle]) {
      return middle;
    }

    if(array.length === 1 && target !== array[middle]) {
      return -1000;
    }

    if (target > array[middle]) {
      return middle + binarySearch(array.slice(middle), target);
    }

    if (target < array[middle]) {
      return binarySearch(array.slice(0, middle), target);
    }
  }

  result = innerFunction(array, target);

  return result > -1 ? result : -1;
}
